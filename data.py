import time
from os import nice, getpid, listdir, path as osp

import torch
import torch.multiprocessing as multiprocessing
from torch_geometric.data import Dataset

from graph import to_nx, from_networkx, neighborhood_split

def process_file(paths):
    in_path, out_path = paths
    print(f"{getpid()}: Processing {in_path} -> {out_path}");
    if osp.exists(out_path) and osp.getmtime(in_path) < osp.getmtime(out_path): return;
    graph, full_skeleton = to_nx(in_path)
    if graph == None:
        print("city to big")
        return
    graph.y = torch.tensor([1])
    full_skeleton.y = torch.tensor([0])
    split_skeleton = neighborhood_split(full_skeleton)
    torch.save((graph, *split_skeleton), out_path)
    print(f"{getpid()}: {out_path} done")

class GeoDataSet(Dataset):
    
    def __init__(self, root=None, transform=None, pre_transform=None,
                 pre_filter=None):
        super(GeoDataSet, self).__init__(root, transform, pre_transform,
                 pre_filter)

    @property
    def raw_file_names(self):
        r"""The name of the files to find in the :obj:`self.raw_dir` folder in
        order to skip the download."""
        
        file_names = []        
        for filename in listdir(self.raw_dir):
            file_names.append(filename)

        return file_names

    @property
    def processed_file_names(self):
        r"""The name of the files to find in the :obj:`self.processed_dir`
        folder in order to skip the processing."""

        file_names = ['pre_filter.pt', 'pre_transform.pt']
        i = 0
        for filename in listdir(self.raw_dir):
            file_names.append('processed_{}.pt'.format(i))
            i = i+1

        return file_names

    def process(self):

        with multiprocessing.Pool(30, initializer = lambda: nice(19)) as pool:
            pool.map(process_file,
                    [(osp.join(self.raw_dir, filename),
                      osp.join(self.processed_dir, f"processed_{i}.pt"))
                     for i, filename
                     in enumerate(listdir(self.raw_dir))]
                    );

    def len(self):
        if len(self.processed_file_names) == 0:
            return len(self.processed_file_names)
        else:
            return len(self.processed_file_names)-2

    def get(self, idx):
        data = torch.load(osp.join(self.processed_dir, "processed_{}.pt".format(idx)))
        return data

    @property
    def num_edge_features(self):
        r"""Returns the number of features per edge in the dataset."""
        return self[0][0].num_edge_features
    
    @property
    def num_node_features(self):
        r"""Returns the number of features per node in the dataset."""
        return self[0][0].num_node_features

def main():
    directory = "C:/Users/Anaklysmos/semesterprojekt-link-prediction/dataset"
    A = GeoDataSet(directory)
    


if __name__ == "__main__":
    main()
