import sys;
import torch;

from torch.nn import Linear, Module, ModuleList, BCELoss, functional as F;
from torch_geometric.nn import GCNConv, global_mean_pool;
from torch_geometric.data import DataLoader;

from data import GeoDataSet;
from graph import *;

# predictor network
class LinkPredictorNetwork(Module):
    def __init__(self, input_size, hidden_sizes, output_size):
        super().__init__();
        self.input_size = input_size;
        self.hidden_sizes = hidden_sizes;
        self.output_size = output_size;

        self.input = GCNConv(self.input_size, self.hidden_sizes[0]);
        self.convs = ModuleList([GCNConv(self.hidden_sizes[i], self.hidden_sizes[i + 1]) for i in range(len(self.hidden_sizes) - 1)]);
        self.output = Linear(self.hidden_sizes[-1], self.output_size);

    def forward(self, data, edge_index, batch_index):
        # print(data.shape, edge_index.shape, batch_index.shape);
        state = self.input(data, edge_index);
        for hidden_layer in self.convs:
            state = F.relu(state);
            state = hidden_layer(state, edge_index);
        state = torch.tanh(state);
        output = self.output(state);
        pool = global_mean_pool(output, batch_index);

        return pool;

#discriminator network
class DiscriminatorNetwork(Module):
    def __init__(self, input_size, hidden_sizes, output_size):
        super().__init__();
        self.input_size = input_size;
        self.hidden_sizes = hidden_sizes;
        self.output_size = output_size;

        self.input = GCNConv(self.input_size, self.hidden_sizes[0]);
        self.convs = ModuleList([GCNConv(self.hidden_sizes[i], self.hidden_sizes[i + 1]) for i in range(len(self.hidden_sizes) - 1)]);
        self.output = Linear(self.hidden_sizes[-1], self.output_size);

    def forward(self, data, edge_index, batch_index):
        # print(data.shape, edge_index.shape, batch_index.shape);
        state = self.input(data, edge_index);
        for hidden_layer in self.convs:
            state = F.relu(state);
            state = hidden_layer(state, edge_index);
        state = torch.tanh(state);
        output = self.output(state);
        output = torch.sigmoid(output);
        pool = global_mean_pool(output, batch_index);

        return pool;

if __name__ == "__main__":

    if len(sys.argv) == 1:
        testmode = False;
    elif len(sys.argv) == 2:
        testmode = "t" in sys.argv[1].lower();
    else:
        print("Wrong number of arguments. Usage: python net.py [test mode (bool)]");
        sys.exit(-1);

    if testmode:
        torch.manual_seed(242424242);
        torch.multiprocessing.set_start_method('spawn');
        torch.multiprocessing.set_sharing_strategy('file_system');
        device = torch.device("cpu");
        print("Running in test mode ..");
    else:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu");

    dataset_path = "/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/dataset" if not testmode else "/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/testdataset"
    #dataset_path = "C:/Users/Anaklysmos/semesterprojekt-link-prediction/dataset"
    loader = DataLoader(GeoDataSet(dataset_path), shuffle = True, exclude_keys = ['batch', 'ptr'], num_workers = 30 if not testmode else 0);

    # eigentlich sollten edge_features verwendet werden
    #linkpredictor = LinkPredictorNetwork(loader.dataset.num_edge_features, [30, 20, 10], 1 + loader.dataset.num_edge_features).to(device);
    #discriminator = DiscriminatorNetwork(loader.dataset.num_edge_features, [100, 50, 20], 1).to(device);
    linkpredictor = LinkPredictorNetwork(loader.dataset.num_features, [30, 20, 10], 1 + loader.dataset.num_features).to(device);
    discriminator = DiscriminatorNetwork(loader.dataset.num_features, [100, 50, 20], 1).to(device);

    lossfn = BCELoss();

    linkpredictor.train();
    discriminator.train();
    link_optimizer = torch.optim.Adam(linkpredictor.parameters(), lr=0.0005);
    disc_optimizer = torch.optim.Adam(discriminator.parameters(), lr=0.0005);

    for batch_num, (real_graphs, skel_batch, stitch_info) in enumerate(loader):
        skel_batch.to(device);
        real_graphs.to(device);
        num_fakes = skel_batch.num_graphs;
        num_reals = real_graphs.num_graphs;

        # train discriminator
        fake_links = linkpredictor(skel_batch.x, skel_batch.edge_index, skel_batch.nested_batch);
        fake_graphs = neighborhood_stitch(fake_links, stitch_info);

        disc_optimizer.zero_grad();

        fake_results = discriminator(fake_graphs.x, fake_graphs.edge_index, fake_graphs.batch);
        real_results = discriminator(real_graphs.x, real_graphs.edge_index, real_graphs.batch);
        fake_loss = lossfn(fake_results, torch.zeros(num_fakes, 1).to(device));
        real_loss = lossfn(real_results, torch.ones(num_reals, 1).to(device));

        fake_loss.backward();
        real_loss.backward();
        disc_optimizer.step();
        disc_loss = fake_loss + real_loss;

        # train linkpredictor
        link_optimizer.zero_grad();

        links = linkpredictor(skel_batch.x, skel_batch.edge_index, skel_batch.nested_batch);
        output = neighborhood_stitch(links, stitch_info);

        results = discriminator(output.x, output.edge_index, output.batch);
        link_loss = lossfn(results, torch.zeros(num_fakes, 1).to(device));

        link_loss.backward();
        link_optimizer.step();

        # print some results
        print(f"Batch {batch_num} finished.\n\tLink predictor loss: {link_loss}\n\tDiscriminator loss: {disc_loss}");
