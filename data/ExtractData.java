import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.List;

/***
 * Berechnet zu einer .osm-Datei die OSM-Daten repräsentiert geeignete .txt-Dateien die dazu verwendet werden können, den
 * Graphen des Straßennetzes der entsprechenden Ortschaft zu generieren.
 *
 * Es werden drei Eingabeattribute benötigt:
 *      input_path: gibt den Pfad der einzulesenden Datei an
 *      output_directory: Pfad des Ordners, in welchem die .txt.-Dateien gespeichert werden sollen
 *      country: Name des Landes aus welchem die OSM-Daten stammen. Mögliche Werte müssen aus {"germany", "france", "italy"} stammen.
 *
 * Die Ausgabedatei hat folgendes Format:
 *    - Einen Kopf der Form
 *          Mindesteinwohnerzahl: ----
 *          Einwohnerzahl (0 falls keine Angabe): ----
 *          Kantenattribute: -----
 *          Knotenattribute: -----
 *    - Eine Leerzeile
 *    - Einen ersten Teil der alle Knoten der Ortschaft enthält. Diese werden angegeben mit ihrer Knoten-ID (die als Zahl selbst keine Bedeutung hat)
 *      sowie ihren Attributwerten wobei die Attribute in der Reihenfolge angegeben sind wie im Kopf der Datei festgehalten. Der Default-Wert aller
 *      Attribute ist 0.
 *    - Eine Leerzeile
 *    - Einen zweiten Teil der alle Kanten der Ortschaft enthält. Diese werden angegeben mit der Knoten-ID ihrer Start- und Endknoten
 *      sowie ihren Attributwerten wobei die Attribute in der Reihenfolge angegeben sind wie im Kopf der Datei festgehalten. Der Default-Wert aller
 *      Attribute ist 0.
 *    - Eine Leerzeile
 *    - Einen dritten Teil der alle Wege innerhalb der Stadt auflistet. Dabei wird zuerst eine Weg-ID angegeben, gefolgt von der Länge des Weges.
 *      Anschließend werden der Reihe nach alle Knoten des Weges mit ihrer Knoten-ID aufgelistet.
 */
public class ExtractData {


    public static void main(String[] args){

        String input_path = args[0];
        String output_directory = args[1];
        String country = args[2];

        try {
            File inputFile = new File(input_path);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHandler uh = new UserHandler(output_directory, country);
            saxParser.parse(inputFile, uh);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

class UserHandler extends DefaultHandler {
    String SAVE_DIRECTORY;
    boolean germany = false;
    boolean france = false;
    boolean italy = false;

    public UserHandler(String output_directory, String country){
        this.SAVE_DIRECTORY = output_directory;

        switch (country){
            case "germany":
                germany=true;
                break;
            case "france":
                france = true;
                break;
            case "italy":
                italy = true;
                break;
            default:
                System.out.println("Fehlerhafte Eingabe für das Land");
        }

    }


    /***
     * Berechnet zu zwei Punkten mit Geo-Koordinaten deren Abstand in km.
     * Input:
     *      (lat1,lon1): Koordinaten des ersten Punktes als double
     *      (lat2,lon2): Koordinaten des zweiten Punktes als double
     * Output:
     *      distance: Abstand der zwei Punkte in km als double
     *
     * Quelle: https://www.kompf.de/gps/distcalc.html
     */
    static double berechne_laenge(double lat1, double lat2, double lon1, double lon2) {
        double lat = ((lat1 + lat2) / 2) * 0.01745;
        double dy = 111.3 * (lat1 - lat2);
        double dx = 111.3 * Math.cos(lat) * (lon1 - lon2);

        double distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }


    Long min_population = 5000L; //minimale Bevölkerung damit eine Stadt betrachtet wird

    int zaehler =0;
    int zaehler_bis_100000 = 0;

    HashMap<Long, String[]> nodes = new HashMap<>(); //Alle Knoten die in der XML-Datei zu finden sind. Key: node_id    Values: lat and lon als String
    HashSet<Long> ways = new HashSet<>(); //Alle "erlaubten" Wege aus der XML-Datei in einer Menge
    HashMap<Long, int[]> way_attributes = new HashMap<>(); //Speichert die Attribute eines Weges. Key: way_id  Values: "att_name:att_value"
    HashMap<Long, Double> way_length = new HashMap<Long, Double>();
    //HashMap<Long, ArrayList<Long>> boundary_way = new HashMap<>(); //Speichert die Wege die ein boundary-Attribut haben
    //HashMap<Long, Integer> admin_level_ways = new HashMap<>(); //Weißt jedem Weg mit boundary-Attribut sein admin_level zu
    HashMap<Long, ArrayList<Long>> all_ways = new HashMap<>(); //speichert alle Wege des XML-Dokumentes um später bei den Relationen darauf zugreifen zu können
    HashMap<Long, ArrayList<Long>> corresponding_ways = new HashMap<>(); //speichert zu einem Knoten alle Wege in denen dieser Knoten vorkommt

    //Speichert die Straßentypen die überhaupt betrachtet werden sollen
    ArrayList<String> allowed_road_types = new ArrayList<>(Arrays.asList("motorway", "trunk", "primary", "secondary", "tertiary", "unclassified",
            "residential", "motorway_link", "trunk_link", "primary_link", "secondary_link", "tertiary_link"));

    //Speichert, welches Element gerade offen ist
    boolean node = false;
    boolean way = false;
    boolean tag = false;
    boolean relation = false;
    boolean nd = false; //für Elemente eines Weges
    boolean member = false; //für Elemente einer Relation

    boolean first_relation = false; //wird wahr sobald die erste Relation gelesen wird. Sobald dies geschieht werden Knoten und Wege vorverarbeitet
    boolean first_way = false; //wird wahr sobal der erste Weg gelesen wird

    //zu überprüfende Werte innerhalb eines Elements
    boolean highway = false;
    boolean visible = false;
    boolean boundary = false;
    boolean name_prefix = false;
    int admin_level = 0;
    Long population = 0L;


    String city_name = "";
    //Long admin_centre = 0L;
    Long way_id = 0L;
    Long relation_id = 0L;
    Polygon gebiet = new Polygon();

    //Nur die Knoten, die auch in einem Weg vorkommen
    HashSet<Long> usedNodes = new HashSet<Long>();

    //Für die Betrachtung von Knoten
    String[] attributesOfNode = new String[5];
    Long node_id=0L;

    //Für die Betrachtung von Wegen
    ArrayList<Long> containedNodesOfWay = new ArrayList<Long>(); //speichert zu einem konkreten Weg temporär die Knoten die dieser Weg enthält
    int[] attributesOfWay = new int[7]; //speichert zu einem konkreten Weg temporär seine Attribute
    String[] nameOfAttributes = {"roadtype", "maxspeed", "lanes", "motorroad", "oneway", "priority_road", "traffic_calming"};

    //Für die Betrachtung von Relationen
    ArrayList<Long> containedWays = new ArrayList<>(); //speichert zu einer Relation die enthaltenen Wege
    ArrayList<Long> ways_in_right_order = new ArrayList<>(); //enthält die Wege aus containedWays in der richtigen Reihenfolge
    HashMap<Long, Long> ways_in_right_order_orientation = new HashMap<>(); //speichert zu den Wegen aus ways_in_right_order ihre Orientierung
    Set<Long> containedNodesOfRel = new HashSet<>(); //speichert zu einem Gebiet einer Relation die Knoten die innerhalb des Gebietes liegen

    //Methode, die bei Beginn eines neuen Elementes im XML-Dokument aufgerufen wird
    @Override
    public void startElement(String uri, String localName, String qName, Attributes att) {
        if (qName.equals("node")) {
            zaehler_bis_100000++;
            if(zaehler_bis_100000>=100000){
                zaehler_bis_100000=0;
                zaehler++;
                System.out.println(zaehler+" mal 100000");
            }
            node = true;
            attributesOfNode[0] = att.getValue("lat");
            attributesOfNode[1] = att.getValue("lon");
            attributesOfNode[2] = ""+0; //traffic_lights
            attributesOfNode[3] = ""+0; //lanes
            attributesOfNode[4] = ""+0; //maxspeed

            node_id = Long.parseLong(att.getValue("id"));
        }
        else if (qName.equals("way")) {
            if(!first_way){
                first_way= true;
                System.out.println("Beginn der Bearbeitung von Wegen");
                zaehler_bis_100000=0;
                zaehler = 0;
            }
            zaehler_bis_100000++;
            if(zaehler_bis_100000>=100000){
                zaehler_bis_100000=0;
                zaehler++;
                System.out.println(zaehler+" mal 100000");
            }
            way = true;
            visible = true; //Eine Annahme, es findet keine Überprüfung im XML-Dokument statt!
            way_id = Long.parseLong(att.getValue("id"));
        }
        else if (qName.equals("nd")) {
            nd = true;
            try {
                containedNodesOfWay.add(Long.parseLong(att.getValue("ref")));
            } catch (Exception e) {
                System.out.println("Fehler beim Lesen der Referenz eines Elements nd");
            }
        }
        else if (qName.equals("tag")) {
            tag = true;
            if (way) {
                if (att.getValue("k").equals("highway")) {
                    highway = true;
                    if (!allowed_road_types.contains(att.getValue("v"))) {
                        highway = false; //falls es sich um keinen erlaubten Straßentypen handelt wird der Weg nicht betrachtet
                    } else {
                        attributesOfWay[0]=allowed_road_types.indexOf(att.getValue("v"));
                    }
                } else if (att.getValue("k").equals("boundary")) {
                    boundary = true;
                } else if (att.getValue("k").equals("admin_level")) {
                    try {
                        admin_level = Integer.parseInt(att.getValue("v"));
                    } catch (Exception e) {
                        System.out.println("Ein admin_level-Tag hatte keinen Integer als Wert");
                    }
                }

                if (highway) {
                    if (att.getValue("k").equals("maxspeed")) {
                        String[] speed_value = att.getValue("v").split(" ");
                        String[] forbidden_types_array = {"none", "walk", "signals", "FR:rural", "FR:urban", "FR:zone30", "FR:motorway",
                                "DE:urban", "DE:rural", "DE:living_street", "DE:bicycle_road", "DE:motorway",  "DE:walk" ,
                                "IT:urban", "IT:rural", "IT:trunk", "IT:motorway"};
                        List<String> forbidden_types = Arrays.asList(forbidden_types_array);
                        if (speed_value.length == 1 && !forbidden_types.contains(att.getValue("v")) ) {
                            try {
                                attributesOfWay[1]=Integer.parseInt(speed_value[0]);
                            } catch (Exception e) {
                                System.out.println("Fehler bei Maximalgeschwindigkeit");
                                for(int i=0; i<speed_value.length; i++){
                                    System.out.print(speed_value[i]+" ");
                                }
                                System.out.println("");
                            }
                        }
                        else if(speed_value.length==2 && speed_value[1].equals("mph")){
                            try {
                                attributesOfWay[1]=(int) Math.round(1.609344*Integer.parseInt(speed_value[0]));
                            } catch (Exception e) {
                                System.out.println("Fehler bei Maximalgeschwindigkeit");
                                for(int i=0; i<speed_value.length; i++){
                                    System.out.print("_"+speed_value[i]);
                                }
                                System.out.println("");
                            }
                        }
                        else if(germany){
                            if(speed_value.length==1){
                                switch (speed_value[0]) {
                                    case "DE:urban": attributesOfWay[1] = 50;
                                        break;
                                    case "DE:rural": attributesOfWay[1] = 100;
                                        break;
                                    case "DE:living_street": attributesOfWay[1] = 7;
                                        break;
                                    case "DE:bicycle_road": attributesOfWay[1] = 30;
                                        break;
                                }
                            }
                        }
                        else if(france){
                            if(speed_value.length==1){
                                switch (speed_value[0]){
                                    case "FR:urban": attributesOfWay[1] = 50;
                                        break;
                                    case "FR:rural": attributesOfWay[1] = 80;
                                        break;
                                    case "FR:zone30": attributesOfWay[1] = 30;
                                        break;
                                    case "FR:motorway": attributesOfWay[1] = 130;
                                }
                            }
                        }
                        else if (italy){
                            if(speed_value.length==1){
                                switch (speed_value[0]){
                                    case "IT:urban": attributesOfWay[1] = 50;
                                        break;
                                    case "IT:rural": attributesOfWay[1] = 90;
                                        break;
                                    case "IT:trunk": attributesOfWay[1] = 110;
                                        break;
                                    case "IT:motorway": attributesOfWay[1] = 130;
                                }
                            }
                        }
                    }
                    if (att.getValue("k").equals("lanes")) {
                        try {
                            attributesOfWay[2]=Integer.parseInt(att.getValue("v"));
                        } catch (Exception e) {
                            System.out.println("Fehler bei Spuranzahl");
                        }
                    }
                    if (att.getValue("k").equals("motorroad") && att.getValue("v").equals("yes")) {
                        attributesOfWay[3]=1;
                    }
                    if (att.getValue("k").equals("oneway") && (att.getValue("v").equals("yes") || att.getValue("v").equals("1") || att.getValue("v").equals("true"))) {
                        attributesOfWay[4] = 1;
                    }
                    if (att.getValue("k").equals("priority_road")) {
                        if (att.getValue("v").equals("designated") || att.getValue("v").equals("yes_unposted")) {
                            attributesOfWay[5] = 1;
                        }
                    }
                    if (att.getValue("k").equals("traffic_calming")) {
                        attributesOfWay[6]= 1;
                    }
                }

            }
            else if (relation) {
                try {
                    if (att.getValue("k").equals("population")) {
                        population = Long.parseLong(att.getValue("v"));
                    }
                    if (att.getValue("k").equals("name")) {
                        city_name = att.getValue("v");
                        String[] split_at_slash = city_name.split("/");
                        if (split_at_slash.length > 1) {
                            city_name = "";
                            for (int i = 0; i < split_at_slash.length; i++) {
                                city_name += split_at_slash[i];
                            }
                        }

                    }
                    if (att.getValue("k").equals("name:prefix")) {
                        name_prefix = true;
                        HashSet<String> list_of_prefixes = new HashSet<>();
                        list_of_prefixes.add("Stadt");
                        list_of_prefixes.add("Kreisstadt");
                        list_of_prefixes.add("Landeshauptstadt");
                        list_of_prefixes.add("Kreisfreie Stadt");

                        list_of_prefixes.add("Freie und Hansestadt");       list_of_prefixes.add("Büchnerstadt");                   list_of_prefixes.add("Kolpingstadt");                       list_of_prefixes.add("Liebenbachstadt");
                        list_of_prefixes.add("Brüder-Grimm-Stadt");         list_of_prefixes.add("Barbarossastadt");                list_of_prefixes.add("Stadt der FernUniversität");          list_of_prefixes.add("Lutherstadt");
                        list_of_prefixes.add("Hochschulstadt");             list_of_prefixes.add("Münchhausenstadt");               list_of_prefixes.add("Friedrich-Ludwig-Weidig-Stadt");      list_of_prefixes.add("Stadt und Landgemeinde");
                        list_of_prefixes.add("Hansestadt");                 list_of_prefixes.add("Einheitsgemeinde und Stadt");     list_of_prefixes.add("Die Mähdrescherstadt");               list_of_prefixes.add("Berg- und Universitätsstadt");
                        list_of_prefixes.add("Universitätsstadt");          list_of_prefixes.add("Sickingenstadt, Stadt");          list_of_prefixes.add("Fundort des Neanderthalers, Stadt");  list_of_prefixes.add("Blütenstadt");
                        list_of_prefixes.add("Große Kreisstadt");           list_of_prefixes.add("Klingenstadt");                   list_of_prefixes.add("Stadt der Burgmannshöfe");            list_of_prefixes.add("Land und Kreisfreie Stadt");
                        list_of_prefixes.add("Goethestadt");                list_of_prefixes.add("Kreis- und Hochschulstadt");      list_of_prefixes.add("Schloss-Stadt");                      list_of_prefixes.add("Dom- und Kaiserstadt");
                        list_of_prefixes.add("Wissenschaftsstadt");         list_of_prefixes.add("Stadt der Fernuniversität");      list_of_prefixes.add("Stadt der Kluterthöhle");             list_of_prefixes.add("Reuterstadt, Stadt");
                        list_of_prefixes.add("Hansestadt an der Ruhr");     list_of_prefixes.add("Stadt auf der Höhe");             list_of_prefixes.add("Freie Hansestadt");                   list_of_prefixes.add("Reformationsstadt, Kreisstadt");
                        list_of_prefixes.add("Paul-Gerhard-Stadt");         list_of_prefixes.add("Orgelstadt");                     list_of_prefixes.add("Welterbestadt");                      list_of_prefixes.add("Widukindstadt");
                        list_of_prefixes.add("Konrad-Zuse-Stadt");          list_of_prefixes.add("Glockenstadt");                   list_of_prefixes.add("Mühlenstadt");                        list_of_prefixes.add("Kupferstadt");
                        list_of_prefixes.add("Schöfferstadt");              list_of_prefixes.add("Karolingerstadt");                list_of_prefixes.add("Stadt der Osterräder");

                        if (!list_of_prefixes.contains(att.getValue("v"))) {
                            relation = false;
                            name_prefix = false;
                        }
                    }
                    if (att.getValue("k").equals("admin_level")) {
                        admin_level = Integer.parseInt(att.getValue("v"));
                    }
                    if (att.getValue("k").equals("boundary")) {
                        if (!att.getValue("v").equals("administrative")) {
                            relation = false;
                        } else {
                            boundary = true;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Fehler beim Behandeln eines Relation-tags " + relation_id);
                }
            }
            else if (node){
                if(att.getValue("k").equals("highway") &&att.getValue("v").equals("traffic_signals")){
                    attributesOfNode[2] = ""+1;
                }
            }
        }
        else if (qName.equals("relation")) {
            relation = true;
            relation_id = Long.parseLong(att.getValue("id"));
            if (!first_relation) {
                first_relation = true;
                System.out.println("Bearbeitung von Relationen hat begonnen");

                //Betrachte lediglich Knoten die auch in einem Weg vorkommen
                for (Long i : ways) {
                    for (Long j : all_ways.get(i)) {
                        if (nodes.containsKey(j)) { //betrachte nur Knoten die auch Koordinaten haben
                            usedNodes.add(j);
                        }
                    }
                }
                System.out.println("usedNodes wurden berechnet");

                //Adjacency Lists erstellen
                HashMap<Long, ArrayList<Long>> adjacencyList = new HashMap<Long, ArrayList<Long>>();
                for (Long i : usedNodes) {
                    ArrayList<Long> neighbors = new ArrayList<>();
                    adjacencyList.put(i, neighbors);
                }
                for (Long i : ways) { //iteriert über alle Wege
                    for (int j = 0; j < all_ways.get(i).size(); j++) { //iteriert über alle Knoten eines Weges
                        if (j == 0 && all_ways.get(i).size() > 1) {
                            adjacencyList.get(all_ways.get(i).get(j)).add(all_ways.get(i).get(j + 1));
                        } else if (j == all_ways.get(i).size() - 1) {
                            adjacencyList.get(all_ways.get(i).get(j)).add(all_ways.get(i).get(j - 1));
                        } else {
                            adjacencyList.get(all_ways.get(i).get(j)).add(all_ways.get(i).get(j - 1));
                            adjacencyList.get(all_ways.get(i).get(j)).add(all_ways.get(i).get(j + 1));
                        }
                    }
                }
                System.out.println("Adjazenzlisten wurden berechnet");

                //Vereinfache die Wege: wenn innerhalb eines ways ein Knoten nur zwei Nachbarn hat so lösche diesen Knoten
                for (Long i : ways) { //iteriert über alle Wege
                    for (int j = all_ways.get(i).size() - 2; j >= 1; j--) { //iteriert in einem Weg über alle Knoten
                        long j_id = all_ways.get(i).get(j); //Id des Knoten der betrachtet werden soll
                        if (adjacencyList.get(j_id).size() == 2) {
                            //passe Adjazenzliste an
                            long neighbor1 = adjacencyList.get(j_id).get(0);
                            long neighbor2 = adjacencyList.get(j_id).get(1);

                            if (!adjacencyList.get(neighbor1).contains(neighbor2)) {
                                adjacencyList.get(neighbor1).add(neighbor2);
                                adjacencyList.get(neighbor2).add(neighbor1);
                            }
                            adjacencyList.get(neighbor1).remove(j_id);
                            adjacencyList.get(neighbor2).remove(j_id);

                            adjacencyList.remove(j_id);

                            //passe usedNodes an
                            usedNodes.remove(j_id);

                            //entferne den Knoten aus dem Weg
                            all_ways.get(i).remove(j);
                        }
                    }
                }
                System.out.println("Wegverarbeitung hat stattgefunden");

                //Berechner corresponding_ways
                for(Long i: nodes.keySet()){
                    ArrayList<Long> ways_to_node = new ArrayList<>();
                    corresponding_ways.put(i, ways_to_node);
                }
                for(Long i: ways){
                    for(Long j: all_ways.get(i)){ //iteriert über alle Knoten in Weg i
                        if(corresponding_ways.get(j).size()>0 &&!corresponding_ways.get(j).contains(i)){
                            corresponding_ways.get(j).add(i);
                        }else{
                            corresponding_ways.get(j).add(i);
                        }
                    }
                }
                System.out.println("corresponding_ways wurde berechnet");

                //Berechne die Attribute maxspeed und lanes für die Knoten
                for(Long i: usedNodes){ //iteriere über alle Knoten
                    int nachbarn_maxspeed = adjacencyList.get(i).size();
                    int nachbarn_lanes = adjacencyList.get(i).size();
                    int average_maxspeed = 0;
                    int average_lanes = 0;

                    for(Long j: corresponding_ways.get(i)){//iteriert über alle Wege die den Knoten i enthalten
                        if(all_ways.get(j).contains(i)){
                            if(all_ways.get(j).indexOf(i)==0 || all_ways.get(j).indexOf(i)==all_ways.get(j).size()-1){
                                average_maxspeed+=way_attributes.get(j)[1];
                                average_lanes+=way_attributes.get(j)[2];
                                if(way_attributes.get(j)[1]==0) {
                                    nachbarn_maxspeed--;
                                }
                                if(way_attributes.get(j)[2]==0){
                                    nachbarn_lanes--;
                                }
                            }
                            else{
                                average_maxspeed+=2*way_attributes.get(j)[1];
                                average_lanes+=2*way_attributes.get(j)[2];
                                if(way_attributes.get(j)[1]==0) {
                                    nachbarn_maxspeed-=2;
                                }
                                if(way_attributes.get(j)[2]==0){
                                    nachbarn_lanes-=2;
                                }
                            }
                        }
                    }
                    if(nachbarn_lanes>0) {
                        nodes.get(i)[3] = "" + average_lanes / nachbarn_lanes;
                    }
                    if(nachbarn_maxspeed>0) {
                        nodes.get(i)[4] = "" + average_maxspeed / nachbarn_maxspeed;
                    }

                }
            }
        }
        else if (qName.equals("member")) {
            member = true;
            try {
                if (att.getValue("type").equals("way") && !att.getValue("role").equals("inner")) {
                    containedWays.add(Long.parseLong(att.getValue("ref")));
                }
                /*else if(att.getValue("type").equals("node")){
                    if(admin_centre>0L){
                        System.out.println("Es gab bei einer Relation mehrere Zentren");
                        relation = false;
                    }
                    else{
                        admin_centre=Long.parseLong(att.getValue("ref"));
                    }
                }*/
            } catch (Exception e) {
                System.out.println("Fehler beim Einlesen eines Members");
            }
        }
    }

    //Methode, die bei Ende eines Elementes im XML-Dokument aufgerufen wird
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("node")) {
            node = false;
            nodes.put(node_id, attributesOfNode);
            node_id = 0L;
            attributesOfNode = new String[5];

        }
        else if (qName.equals("way")) {
            all_ways.put(way_id, containedNodesOfWay);

            if (visible && highway) {
                ways.add(way_id);
                way_attributes.put(way_id, attributesOfWay);
            } else if (boundary) {
                //boundary_way.put(way_id, containedNodesOfWay);
                //admin_level_ways.put(way_id, admin_level);
            }
            visible = false;
            highway = false;
            way = false;
            boundary = false;
            admin_level = 0;
            way_id = 0L;
            containedNodesOfWay = new ArrayList<>();
            attributesOfWay = new int[7];
        }
        else if (qName.equals("nd")) {
            nd = false;
        }
        else if (qName.equals("tag")) {
            tag = false;
        }
        else if (qName.equals("member")) {
            member = false;
        }
        else if (qName.equals("relation")) {
            boolean test_ob_im_gebiet = true;
            if (germany && !name_prefix) {
                relation = false;
            }
            if (relation && boundary) {
                if (((admin_level == 8 )||(germany && admin_level==6))&& (population > min_population || population == 0)) { // in diesem Fall soll aus der Relation eine Stadt extrahiert werden

                    //bringe die Wege aus containedWays in die richtige Reihenfolge
                    for(int j=0; j<containedWays.size(); j++){ //überprüfe ob alle Wege auch im XML-Dokument vorkommen
                        if (!all_ways.containsKey(containedWays.get(j))) {
                            test_ob_im_gebiet = false;
                        }
                    }
                    int number_of_ways = containedWays.size();
                    ways_in_right_order.add(containedWays.get(0));
                    ways_in_right_order_orientation.put(containedWays.get(0), 1L);
                    Long last_way_id = containedWays.get(0);
                    containedWays.remove(0);
                    boolean found = true; //überprüft, ob ein nächstes Element gefunden wurde

                    if (test_ob_im_gebiet) {
                        for (int i = 1; i < number_of_ways; i++) { //fülle die Liste ways_in_right_order, i=Index der gefüllt werden soll
                            found = false;
                            Long last_node = 0L;
                            if (ways_in_right_order_orientation.get(last_way_id).equals(1L)) {
                                last_node = all_ways.get(last_way_id).get(all_ways.get(last_way_id).size() - 1); //Knoten für den Anschluss gesucht wird
                            }
                            else{
                                last_node = all_ways.get(last_way_id).get(0); //Knoten für den Anschluss gesucht wird
                            }
                            if(last_node==0L){
                                test_ob_im_gebiet= false;
                            }
                            for (int j = 0; j < containedWays.size(); j++) { //iteriere über alle noch verbleibenden Wege die passen könnten
                                if (all_ways.get(containedWays.get(j)).get(0).equals(last_node)) {
                                    ways_in_right_order.add(containedWays.get(j));
                                    ways_in_right_order_orientation.put(containedWays.get(j), 1L);
                                    last_way_id = containedWays.get(j);
                                    containedWays.remove(j);
                                    found = true;
                                    break;
                                }
                                else if(all_ways.get(containedWays.get(j)).get(all_ways.get(containedWays.get(j)).size()-1).equals(last_node)) {
                                    ways_in_right_order.add(containedWays.get(j));
                                    ways_in_right_order_orientation.put(containedWays.get(j), 2L);
                                    last_way_id = containedWays.get(j);
                                    containedWays.remove(j);
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                test_ob_im_gebiet = false;
                                break;
                            }
                        }
                    }

                    if(test_ob_im_gebiet && !(ways_in_right_order.size()==number_of_ways)){
                        test_ob_im_gebiet=false;
                        throw new RuntimeException("Fehler bei der Ordnung der Wege - kann nicht auftreten wenn nicht etwas grundlegend schief gelaufen ist");
                    }


                    //Definiere aus den Weggrenzen das Polygon welches die gesuchte Fläche beschreibt
                    int knotenzahl = 0;
                    for (Long i : ways_in_right_order) {
                        try {
                            knotenzahl += all_ways.get(i).size();
                        } catch (Exception e) {
                            System.out.println("Ein Weg " + i + " lag nicht innerhalb der Karte");
                            test_ob_im_gebiet = false;
                            break;
                        }
                    }

                    if (test_ob_im_gebiet) {

                        int[] x_koordinates = new int[knotenzahl];
                        int[] y_koordinates = new int[knotenzahl];
                        int aktueller_index = 0;

                        //es sollen die maximalen Koordinaten des Polygons in jede Himmelsrichtung bestimmt werden
                        int max_lat = Integer.MIN_VALUE;
                        int min_lat = Integer.MAX_VALUE;
                        int max_lon = Integer.MIN_VALUE;
                        int min_lon = Integer.MAX_VALUE;

                        //Bestimmen der x- und y-Koordinaten der Punkte die das Polygon definieren. Diese müssen Integer sein also werden sie in solche umgewandelt
                        for (Long i : ways_in_right_order) { //iteriert über alle Wege die die Grenze bestimmen
                            try {
                                if(ways_in_right_order_orientation.get(i).equals(1L)) {
                                    for (Long j : all_ways.get(i)) {
                                        String[] lat = nodes.get(j)[0].split("\\.");
                                        String x_wert = lat[0] + lat[1];
                                        for (int k = 0; k < 7 - lat[1].length(); k++) {
                                            x_wert += "0";
                                        }
                                        int x = Integer.parseInt(x_wert);

                                        String[] lon = nodes.get(j)[1].split("\\.");
                                        String y_wert = lon[0] + lon[1];
                                        for (int k = 0; k < 7 - lon[1].length(); k++) {
                                            y_wert += "0";
                                        }
                                        int y = Integer.parseInt(y_wert);

                                        x_koordinates[aktueller_index] = x;
                                        y_koordinates[aktueller_index] = y;
                                        aktueller_index++;

                                        if (max_lat < x) {
                                            max_lat = x;
                                        }
                                        if (min_lat > x) {
                                            min_lat = x;
                                        }
                                        if (max_lon < y) {
                                            max_lon = y;
                                        }
                                        if (min_lon > y) {
                                            min_lon = y;
                                        }
                                    }
                                }
                                else{
                                    for (int q= all_ways.get(i).size()-1; q>=0; q--) {
                                        Long j=all_ways.get(i).get(q);
                                        String[] lat = nodes.get(j)[0].split("\\.");
                                        String x_wert = lat[0] + lat[1];
                                        for (int k = 0; k < 7 - lat[1].length(); k++) {
                                            x_wert += "0";
                                        }
                                        int x = Integer.parseInt(x_wert);

                                        String[] lon = nodes.get(j)[1].split("\\.");
                                        String y_wert = lon[0] + lon[1];
                                        for (int k = 0; k < 7 - lon[1].length(); k++) {
                                            y_wert += "0";
                                        }
                                        int y = Integer.parseInt(y_wert);

                                        x_koordinates[aktueller_index] = x;
                                        y_koordinates[aktueller_index] = y;
                                        aktueller_index++;

                                        if (max_lat < x) {
                                            max_lat = x;
                                        }
                                        if (min_lat > x) {
                                            min_lat = x;
                                        }
                                        if (max_lon < y) {
                                            max_lon = y;
                                        }
                                        if (min_lon > y) {
                                            min_lon = y;
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("Something went horribly wrong here");
                            }
                        }

                        //Erstellen des Polygons
                        gebiet = new Polygon(x_koordinates, y_koordinates, knotenzahl);


                        //Schreibe die Daten in eine Datei
                        try {
                            File datei = new File(SAVE_DIRECTORY + city_name + "_" + population + ".txt");
                            //File datei = new File(city_name + "_" + population + ".txt");
                            if (datei.createNewFile()) {
                                System.out.println("File created:" + datei.getName());
                                FileWriter myWriter = new FileWriter(datei);

                                //Bestimme alle Knoten die innerhalb des Stadtgebietes liegen
                                for (Long i : usedNodes) {
                                    String[] lat = nodes.get(i)[0].split("\\.");
                                    String[] lon = nodes.get(i)[1].split("\\.");


                                    String x_wert = lat[0] + lat[1];
                                    for (int k = 0; k < 7 - lat[1].length(); k++) {
                                        x_wert += "0";
                                    }
                                    int x = Integer.parseInt(x_wert);

                                    String y_wert = lon[0] + lon[1];
                                    for (int k = 0; k < 7 - lon[1].length(); k++) {
                                        y_wert += "0";
                                    }
                                    int y = Integer.parseInt(y_wert);

                                    try {
                                        if(x<=max_lat && x>=min_lat && y<=max_lon && y>=min_lon) {
                                            if (gebiet.contains(x, y)) {
                                                containedNodesOfRel.add(i);
                                            }
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Polygon existiert nicht");
                                    }

                                }

                                //Schreibe Header
                                myWriter.write("Mindesteinwohnerzahl: "+min_population);
                                myWriter.write(System.getProperty("line.separator"));
                                myWriter.write("Einwohnerzahl (0 falls keine Angabe): "+population);
                                myWriter.write(System.getProperty("line.separator"));
                                myWriter.write("Kantenattribute:");
                                for(int i=0; i<nameOfAttributes.length; i++){
                                    myWriter.write(" "+nameOfAttributes[i]);
                                }
                                myWriter.write(System.getProperty("line.separator"));
                                myWriter.write("Knotenattribute: lat lon traffic_signals average_lanes average_maxspeed");
                                myWriter.write(System.getProperty("line.separator"));
                                myWriter.write(System.getProperty("line.separator"));


                                //Schreibe die Knoten und deren Koordinaten in die Datei
                                for (Long i : containedNodesOfRel) {
                                    myWriter.write(i + " " + nodes.get(i)[0] + " " + nodes.get(i)[1]+" "+ nodes.get(i)[2]+" "+nodes.get(i)[3]+" "+nodes.get(i)[4]);
                                    myWriter.write(System.getProperty("line.separator"));
                                }
                                //schreibe Trennlinie zwischen Knoten und Kanten in die Datei
                                myWriter.write(System.getProperty("line.separator"));

                                //schreibe Kanten und deren Attribute in die Datei
                                for (Long i : ways) { //iteriert über alle Wege
                                    ArrayList<Long> nbs = all_ways.get(i); //Liste aller Knoten des Weges i
                                    for (int j = 0; j < nbs.size() - 1; j++) { //iteriert über alle Knoten eines Weges
                                        //nur wenn die Knoten einer Kante auch innerhalb des Gebietes liegen, schreibe diese Kante
                                        if (containedNodesOfRel.contains(nbs.get(j)) && containedNodesOfRel.contains(nbs.get(j + 1))) {
                                            //schreibt Start- und Endknoten
                                            myWriter.write(nbs.get(j) + " " + nbs.get(j + 1));

                                            //schreibe Länge als Attribut
                                            double lat1 = Double.parseDouble(nodes.get(nbs.get(j))[0]);
                                            double lon1 = Double.parseDouble(nodes.get(nbs.get(j))[1]);
                                            double lat2 = Double.parseDouble(nodes.get(nbs.get(j + 1))[0]);
                                            double lon2 = Double.parseDouble(nodes.get(nbs.get(j + 1))[1]);
                                            double distance = berechne_laenge(lat1, lat2, lon1, lon2);
                                            myWriter.write(" "  + distance);

                                            //update way_length
                                            if(way_length.keySet().contains(i)){
                                                way_length.put(i, way_length.get(i)+distance);
                                            }
                                            else{
                                                way_length.put(i, distance);
                                            }

                                            //schreibt Attribute
                                            for (int k = 0; k < way_attributes.get(i).length; k++) {
                                                myWriter.write(" "+ way_attributes.get(i)[k]);
                                            }
                                            myWriter.write(System.getProperty("line.separator"));
                                        }
                                    }
                                }

                                myWriter.write(System.getProperty("line.separator"));

                                //Schreibe Wege, deren Knoten und Länge in die Datei
                                for(Long i: way_length.keySet()){
                                    myWriter.write(i + " "+ way_length.get(i));
                                    for(int j=0; j<all_ways.get(i).size(); j++){
                                        if(containedNodesOfRel.contains(all_ways.get(i).get(j))) {
                                            myWriter.write(" " + all_ways.get(i).get(j));
                                        }
                                    }
                                    myWriter.write(System.getProperty("line.separator"));
                                }

                                myWriter.flush();
                                myWriter.close();
                            } else {
                                System.out.println("File already exists");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            way_length = new HashMap<Long, Double>();
            relation = false;
            //admin_centre = 0L;
            containedWays = new ArrayList<Long>();
            ways_in_right_order = new ArrayList<>();
            ways_in_right_order_orientation = new HashMap<Long, Long>();
            containedNodesOfRel = new HashSet<>();
            boundary = false;
            population = 0L;
            admin_level = 0;
            city_name = "";
            relation_id = 0L;
            gebiet = new Polygon();
            name_prefix = false;
        }
    }
}
