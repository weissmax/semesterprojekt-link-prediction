import os.path
import sys


input_directory = sys.argv[1]
output_directory_files = sys.argv[2]
output_directory_adjacency_lists = sys.argv[3]


def update_file(filename):
    print(filename)
    if os.path.isdir(input_directory + filename):
        return
    if not os.path.isfile(input_directory + filename):
        print("File does not exist")
        exit(-1)
    else:
        with open(input_directory + filename, 'r') as f:
            content = f.readlines()

    node_to_node = dict()
    adjacency_list = dict()
    leerzeile_gelesen1 = False
    leerzeile_gelesen2 = False
    leerzeile_gelesen3 = False
    leerzeile_gelesen4 = False
    number_of_edges = 0
    index = 1

    new_file = open(output_directory_files + filename, "w")
    adjacency_list_file = open(output_directory_adjacency_lists + filename, "w")

    for line in content:
        if line == "\n":
            new_file.write(line)
            if not leerzeile_gelesen1:
                leerzeile_gelesen1 = True
            elif not leerzeile_gelesen2:
                leerzeile_gelesen2 = True
            elif not leerzeile_gelesen3:
                leerzeile_gelesen3 = True
            elif not leerzeile_gelesen4:
                leerzeile_gelesen4 = True
        else:
            linesplit = line.split(" ")
            if not leerzeile_gelesen1:  # im Kopf der Datei
                new_file.write(line)
            elif not leerzeile_gelesen2:  # im Knotenteil der Datei
                if not linesplit[0] in node_to_node:
                    node_to_node[linesplit[0]] = index
                    adjacency_list[index] = []
                    index = index + 1
                new_file.write(str(node_to_node.get(linesplit[0])))
                for i in range(1, len(linesplit)):
                    new_file.write(" " + str(linesplit[i]))
            elif not leerzeile_gelesen3:  # im Kantenteil der Datei
                number_of_edges = number_of_edges+1
                new_file.write(str(node_to_node.get(linesplit[0])))
                new_file.write(" " + str(node_to_node.get(linesplit[1])))
                adjacency_list.get(node_to_node.get(linesplit[0])).append(node_to_node.get(linesplit[1]))
                adjacency_list.get(node_to_node.get(linesplit[1])).append(node_to_node.get(linesplit[0]))
                for i in range(2, len(linesplit)):
                    new_file.write(" " + linesplit[i])
            elif not leerzeile_gelesen4:  # im Wegeteil der Datei
                new_file.write(linesplit[0] + " " + linesplit[1])
                for i in range(2, len(linesplit)):
                    if i == len(linesplit) - 1:
                        linesplit2 = linesplit[i].split("\n")
                        new_file.write(" " + str(node_to_node.get(linesplit2[0])))
                    else:
                        new_file.write(" " + str(node_to_node.get(linesplit[i])))
                new_file.write("\n")
            else:
                return

    adjacency_list_file.write(str(index - 1) + " " + str(number_of_edges) + "\n")
    for i in range(1, index):
        if len(adjacency_list.get(i))>0:
            adjacency_list_file.write(str(adjacency_list.get(i)[0]))
        else:
            print(i)
        for j in range(1, len(adjacency_list.get(i))):
            adjacency_list_file.write(" " + str(adjacency_list.get(i)[j]))
        if not i == index - 1:
            adjacency_list_file.write("\n")


for filename in os.listdir(input_directory):
    update_file(filename)
